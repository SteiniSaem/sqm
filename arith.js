const add = (a, b) => a + b;
const mul = (a, b) => a * b;
const sub = (a, b) => a - b;
const div = (a, b) => a / b;

const dummy = (a, b, c) => {
  var d = Math.pow(b, 2) - 4 * a * c;
  if (d < 0) {
    return null;
  } else if (d >= 0) {
    return ((-1 * b + Math.sqrt(d)) / 2) * a;
  }
};

module.exports = { add, mul, sub, div, dummy };
