[![pipeline status](https://gitlab.com/SteiniSaem/sqm/badges/master/pipeline.svg)](https://gitlab.com/SteiniSaem/sqm/-/commits/master)
[![coverage report](https://gitlab.com/SteiniSaem/sqm/badges/master/coverage.svg)](https://gitlab.com/SteiniSaem/sqm/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=SteiniSaem_sqm&metric=alert_status)](https://sonarcloud.io/dashboard?id=SteiniSaem_sqm)

This project is a react-native app where you can make awesome lists.
You can run the app on a smartphone emulator or on your own smartphone. To run on your phone you need the expo client app. To build and run the project you just need to enter in the command line the command "npm start". It will open a tab in your browser where you can scan a QR code with your phone and that should take you to the expo app where you can open the List app.