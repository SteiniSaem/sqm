const { add, mul, sub, div } = require("../arith");

test("test1", () => {
  expect(add(2, 3)).toBe(5);
});

test("test2", () => {
  expect(mul(3, 4)).toBe(12);
});

test("test3", () => {
  expect(sub(5, 6)).toBe(-1);
});

test("test4", () => {
  expect(div(8, 4)).toBe(2);
});
